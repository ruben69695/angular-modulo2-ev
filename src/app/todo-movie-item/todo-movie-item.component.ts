import { Component, OnInit, HostBinding, Host, Input } from '@angular/core';
import { TodoMovieItem } from '../models/todo-movie-item.models';

@Component({
  selector: 'app-todo-movie-item',
  templateUrl: './todo-movie-item.component.html',
  styleUrls: ['./todo-movie-item.component.css']
})
export class TodoMovieItemComponent implements OnInit {

  @HostBinding('attr.class') cssClass = 'col col-sm-3';
  @Input() movieItem: TodoMovieItem;

  constructor() { }

  ngOnInit() {
  }

  voteUp(): boolean {
    this.movieItem.voteUp();
    return false;
  }

  voteDown(): boolean {
    this.movieItem.voteDown();
    return false;
  }

}
