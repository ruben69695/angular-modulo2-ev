export class TodoItem {
    name: string;
    description: string;
    selected: boolean;
    creationDate: Date;

    constructor(name: string, creationDate: Date, descr: string) {
        this.name = name;
        this.creationDate = creationDate;
        this.description = descr;
        this.selected = false;
    }

    select(): void {
        if (this.selected) {
            this.selected = false;
        } else {
            this.selected = true;
        }
    }
}
