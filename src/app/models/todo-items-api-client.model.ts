import { Injectable } from '@angular/core';
import { TodoItem } from './todo-item.models';
import { Store } from '@ngrx/store';
import { AppState } from './../store/reducers/main-reducer';
import { NewTodoItemAction, DeleteTodoItemsAction } from '../store/actions/todo-items.actions';

@Injectable()
export class TodoItemsApiClient {
    items: TodoItem[];
    constructor(private store: Store<AppState>) {
        this.store
            .select(state => state.todoItems)
            .subscribe((data) => {
                this.items = data.items;
            });
    }

    add(item: TodoItem) {
        this.store.dispatch(new NewTodoItemAction(item));
    }
    delete() {
        this.store.dispatch(new DeleteTodoItemsAction());
    }
}

