import { Injectable } from '@angular/core';
import { TodoMovieItem } from './todo-movie-item.models';
import { Store } from '@ngrx/store';
import { AppState } from '../store/reducers/main-reducer';
import { VoteUpTodoMovieAction, VoteDownTodoMovieAction } from '../store/actions/movies-items.actions';

@Injectable()
export class TodoMoviesApiClient {
    movies: TodoMovieItem[];
    constructor(private store: Store<AppState>) {
        this.store
            .select(state => state.movies)
            .subscribe((data) => {
                this.movies = data.items;
            });
    }
    voteUp(movie: TodoMovieItem) {
        this.store.dispatch(new VoteUpTodoMovieAction(movie));
    }
    voteDown(movie: TodoMovieItem) {
        this.store.dispatch(new VoteDownTodoMovieAction(movie));
    }
    getAll(): TodoMovieItem[] {
        return this.movies;
    }
}

