import { TodoMovieItem } from 'src/app/models/todo-movie-item.models';
import { Action } from '@ngrx/store';

// ACTIONS
export enum TodoMoviesActionTypes {
    VOTE_UP = '[Todo Movie] VOTE UP',
    VOTE_DOWN = '[Todo Movie] VOTE DOWN',
}

export class VoteUpTodoMovieAction implements Action {
    type = TodoMoviesActionTypes.VOTE_UP;
    constructor(public movieItem: TodoMovieItem) {}
}

export class VoteDownTodoMovieAction implements Action {
    type = TodoMoviesActionTypes.VOTE_DOWN;
    constructor(public movieItem: TodoMovieItem) {}
}

export type TodoMoviesActions = VoteUpTodoMovieAction | VoteDownTodoMovieAction;