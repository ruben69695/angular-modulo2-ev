import { TodoMovieItem } from 'src/app/models/todo-movie-item.models';
import { TodoMoviesActions, TodoMoviesActionTypes, VoteUpTodoMovieAction,
    VoteDownTodoMovieAction } from '../actions/movies-items.actions';

// STATES
export interface TodoMoviesState {
    items: TodoMovieItem[];
}

const initializeTodoMoviesState: TodoMoviesState = {
    items: [
        new TodoMovieItem('Star Wars', new Date(), 'This is a little description to test'),
        new TodoMovieItem('The Avengers EndGame', new Date(), 'This is a little description to test'),
        new TodoMovieItem('Sherlock Holmes', new Date(), 'This is a little description to test'),
        new TodoMovieItem('Avatar', new Date(), 'This is a little description to test'),
    ]
};

// REDUCERS
export function reducerTodoMovies(state: TodoMoviesState = initializeTodoMoviesState, action: TodoMoviesActions): TodoMoviesState {
    switch (action.type) {
        case TodoMoviesActionTypes.VOTE_UP:
            (action as VoteUpTodoMovieAction).movieItem.voteUp();
            return {
                ...state
            };
        case TodoMoviesActionTypes.VOTE_DOWN:
            (action as VoteDownTodoMovieAction).movieItem.voteDown();
            return {
                ...state
            };
    }
    return state;
}


