import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { TodoItem } from '../models/todo-item.models';

@Component({
  selector: 'app-add-todo-item-form',
  templateUrl: './add-todo-item-form.component.html',
  styleUrls: ['./add-todo-item-form.component.css']
})
export class AddTodoItemFormComponent implements OnInit {

  @Output() newTodoItemCreated: EventEmitter<TodoItem>;
  formGroup: FormGroup;
  nameLength: number;

  constructor(formBuilder: FormBuilder) {
    this.nameLength = 3;
    this.newTodoItemCreated = new EventEmitter();
    this.formGroup = formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
        this.nameValidator(this.nameLength)
      ])],
      description: ['', Validators.nullValidator]
    });
  }

  ngOnInit() {
  }

  createTodoItem(name: string, description: string): boolean {
    if (name !== '') {
      this.newTodoItemCreated.emit(new TodoItem(name, new Date(), description));
    }
    return false;
  }

  nameValidator(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { nameInvalid: true };
      }
      return null;
    };
  }

}
