import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import {TodoItem } from './../models/todo-item.models';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Output() selectionChanged: EventEmitter<TodoItem>;
  @Input() todoItem: TodoItem;

  constructor() {
    this.selectionChanged = new EventEmitter<TodoItem>();
  }

  ngOnInit() {
  }

  markSelected(): boolean {
    this.todoItem.select();
    this.selectionChanged.emit(this.todoItem);
    return false;
  }

}
